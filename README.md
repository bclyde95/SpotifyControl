# SPOT

### CLI controls for Spotify Linux Client

#### Uses DBus to control the application from the command line

## Installation

* To install just clone this repo and add the following to your .bash-aliases:
```bash
alias spot=/{path to folder}/spotifyControl/spotifyControls.sh
```

## Usage

* Usage: spot x
	* 't' to toggle Play/Pause
  * 'p' to go to Previous track
  * 'n' to go to Next track
  * 'r' to Restart track
  * 's' for Current Song Info

## Requirements
* Spotify Linux Client GUI (Tested on Debian Sid, Manjaro, Ubuntu, and Arch) # may work with other clients
* BASH
