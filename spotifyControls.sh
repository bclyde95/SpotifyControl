#!/bin/bash
# Author: Brandon Clyde
# Github: https://github.com/bclyde95
# Website: https://www.metrid.co

# Credit to github user wandernauta for the Metadata command https://github.com/wandernauta
Get_Metadata(){
	Metadata="$(dbus-send                                                 \
 	--print-reply                                  `# We need the reply.`       \
 	--dest=org.mpris.MediaPlayer2.spotify                                        \
	 /org/mpris/MediaPlayer2                                                        \
	 org.freedesktop.DBus.Properties.Get                                         \
	 string:'org.mpris.MediaPlayer2.Player' string:'Metadata'                    \
	 | grep -Ev "^method"                           `# Ignore the first line.`   \
	 | grep -Eo '("(.*)")|(\b[0-9][a-zA-Z0-9.]*\b)' `# Filter interesting fiels.`\
	 | sed -E '2~2 a|'                              `# Mark odd fields.`         \
	 | tr -d '\n'                                   `# Remove all newlines.`     \
 	 | sed -E 's/\|/\n/g'                           `# Restore newlines.`        \
 	 | sed -E 's/(xesam:)|(mpris:)//'               `# Remove ns prefixes.`      \
 	 | sed -E 's/^"//'                              `# Strip leading...`         \
 	 | sed -E 's/"$//'                              `# ...and trailing quotes.`  \
 	 | sed -E 's/\"+/|/'                             `# Regard "" as seperator.`  \
 	 | sed -E 's/ +/ /g'                            `# Merge consecutive spaces.`\
	)"

    # Filter Metadata for Artist and Song, then echo the Now Playing string
    Artist=$(echo "$Metadata" | sed -n 's/artist|//p')
	Song=$(echo "$Metadata" | sed -n 's/title|//p')
    echo "Now Playing: $Artist - $Song"
}


if [ $# -eq 1 ] ; then      #Check if there is one argument passed

    #if arg is 't' then send .PlayPause command and print message
    if [ "$1" = "t" ] ; then
        dbus-send --print-reply  --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.PlayPause
        echo "Play/Pause Toggled"
        exit 0

    #if arg is 'p', send .Previous twice then print message and Metadata
    elif [ "$1" = "p" ] ; then
        #.Previous command needs to be sent twice to actually go to previous song
        #If the song is more than 1-2 seconds in, one .Previous will just restart the song
        dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Previous && dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Previous
        echo "Previous Song"
        sleep 0.2       #this sleep helps give DBus time to catch up
        # otherwise the Metadata would be wrong 
        echo ""
        Get_Metadata
        exit 0

    #if arg is 'r', send .Previous once then print message and Metadata
    elif [ "$1" = "r" ] ; then
        #.Previous command restarts the song if it's more than 1-2 seconds in
        dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Previous
        echo "Restart Song"
        sleep 0.2       #this sleep helps give DBus time to catch up
        echo ""
		Get_Metadata
        exit 0

    # if arg is 'n', send .Next then print message and Metadata
    elif [ "$1" = "n" ] ; then
        dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Next
        echo "Next Song"
        sleep 0.2       #this sleep helps give DBus time to catch up
        echo ""
		Get_Metadata
        exit 0

    #if arg is 's', print Metadata
    elif [ "$1" = "s" ] ; then
        Get_Metadata
        exit 0

    #Arg was not any of the applicable ones, print error and exit 1
    else
        echo "Invalid Argument: $1"
        echo "  t for Play/Pause"
        echo "  p for Previous"
        echo "  n for Next"
        echo "  r for Restart"
        echo "  s for Song Info"
        exit 1
    fi

# More than one arg was passed, print error and exit 2
elif [ $# -gt 2 ] ; then
    echo "Too many arguments"
    exit 2

# No arg was passed, print usage and instructions then exit 3
else
    echo "usage: spot <arg>"
    echo ""
    echo "--- Spotify Control Util ---"
    echo "  t for Play/Pause"
    echo "  p for Previous"
    echo "  n for Next"
    echo "  r for Restart"
	echo "  s for Song Info"
    exit 3
fi
